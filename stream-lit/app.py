import streamlit as st
import pickle
import numpy as np
model = pickle.load(open('model.pkl','rb'))



st.set_page_config(
    page_title="Prediction App",
    page_icon="🤖",
    layout="centered",
    initial_sidebar_state="expanded",
)

from PIL import Image
image = Image.open('banner.jpeg')

st.image(image,
      use_column_width=True)


def predict_age(age,sex,cp,trestbps,chol,fbs,restecg,thalach,exang,oldpeak,slope,ca,thal):
    input=np.array([[age,sex,cp,trestbps,chol,fbs,restecg,thalach,exang,oldpeak,slope,ca,thal]]).astype(np.float64)
    prediction = model.predict(input)
    return int(prediction)


def main():
    html_temp = """
    <div style="background:#025246 ;padding:10px">
    <h2 style="color:white;text-align:center;">Penyakit Jantung Prediction ML App </h2>
    </div>
    """
    st.markdown(html_temp, unsafe_allow_html = True)

    age = st.text_input("age")
    sex = st.text_input("sex")
    cp = st.text_input("cp")
    trestbps = st.text_input("trestbps")
    chol = st.text_input("chol")
    fbs = st.text_input("fbs")
    restecg = st.text_input("restecg")
    thalach = st.text_input("thalach")
    exang = st.text_input("exang")
    oldpeak = st.text_input("oldpeak")
    slope = st.text_input("slope")
    ca = st.text_input("ca")
    thal = st.text_input("thal")



    safe_html ="""  
      <div style="background-color:#80ff80; padding:10px >
      <h2 style="color:white;text-align:center;"> Anda Tidak Masuk Kedalam Kategori Potensi Penyakit Jantung</h2>
      </div>
    """
    warn_html ="""  
      <div style="background-color:#F4D03F; padding:10px >
      <h2 style="color:white;text-align:center;"> <b>Anda Berpotensi Masuk Kedalam Kategori Penyakit Jantung</b></h2>
      </div>
    """

    if st.button("Lakukan Prediksi"):
        output = predict_age(age,sex,cp,trestbps,chol,fbs,restecg,thalach,exang,oldpeak,slope,ca,thal)
        st.success('Output Is {}'.format(output))

        if output == 0:
            st.markdown(safe_html,unsafe_allow_html=True)
        elif output == 1:
            st.markdown(warn_html,unsafe_allow_html=True)

if __name__=='__main__':
    main()
